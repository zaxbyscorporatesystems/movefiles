﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace MoveFiles
{
    class CodeEmail : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string FyleFrom
        { set; get; }
        public string FyleTo
        { set; get; }
        public string EmailTo
        { set; get; }
        public bool FileExists
        { set; get; }
        /// <summary>Send the email message.</summary>
        /// <returns>True if sent, false if sror encountered.</returns>
        public bool SendEmail()
        {
            bool rslt = true;
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = "ACH file moved";
                StringBuilder sbMessage = new StringBuilder();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("email.zaxbys.com");
                StringBuilder sbRecEmail = new StringBuilder();
                string[] sendTos = EmailTo.Split('|');
                foreach (string eToo in sendTos)
                {
                    message.To.Add(eToo);

                }
                message.From = new System.Net.Mail.MailAddress("ablashaw@zaxbys.com", "ACH files");
                
                message.CC.Add("ayoungblood@zaxbys.com");
                //message.CC.Add("rcolvin@zaxbys.com");
                message.CC.Add("ablashaw@zaxbys.com");
                //sbMessage.Append("-----test test test test test-----");
                //sbMessage.Append(Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
                if (FileExists)
                {
                    sbMessage.Append($@"The ACH file has NOT been moved from {FyleFrom}");
                    sbMessage.Append(Environment.NewLine);
                    sbMessage.Append(Environment.NewLine);
                    sbMessage.Append($@"To {FyleTo} because it has previously been moved.");
                }
                else
                {
                    sbMessage.Append($@"The ACH file has been moved from {FyleFrom}");
                    sbMessage.Append(Environment.NewLine);
                    sbMessage.Append(Environment.NewLine);
                    sbMessage.Append($@"To {FyleTo}");
                }
                message.Priority = MailPriority.High;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Body = sbMessage.ToString();
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.Send(message);

                smtp.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }

    }
}
