﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MoveFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            string zflFilePrefix = "ZFL",
                zFromDir = @"\\balmorra\lsfprod\law\prod\work\AP160ACHTAPE",
                zflToDir = @"\\Amidala\store\Tracy\Connie",
                zaxFilePrefix = "ZAX",
                zaxToDir = @"\\Amidala\store\Tracy\Renee\R_Tracy_Renee_Infor",
                logFyle =$@"{Environment.CurrentDirectory}\logs\logfyle_{DateTime.Now.DayOfWeek}.txt",
                fyleFrom=string.Empty,
                fyleToo=string.Empty;
            bool bMoved = false;
            try
            {
                if(!Directory.Exists(Path.GetDirectoryName(logFyle)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(logFyle));
                }
                File.AppendAllText(logFyle,$@"Start: {DateTime.Now}"+Environment.NewLine);
                string[] fyles = Directory.GetFiles(zFromDir, "*.");
                foreach(string fyle in fyles)
                {
                    if(!fyle.ToUpper().Contains("_SEQ"))
                    {
                        if(Path.GetFileName(fyle.ToUpper()).StartsWith(zaxFilePrefix))
                        {
                            try
                            {
                                fyleToo = $@"{zaxToDir}\{Path.GetFileName(fyle)}";
                                if (File.Exists(fyleToo))
                                {
                                    bMoved = true;
                                    File.AppendAllText(logFyle, $@"{zaxFilePrefix} File NOT copied from {fyle} to {fyleToo}, moved previously." + Environment.NewLine);
                                }
                                else
                                {
                                    bMoved = false;
                                    File.Copy(fyle, fyleToo);
                                    File.AppendAllText(logFyle, $@"{zaxFilePrefix} File copied from {fyle} to {fyleToo}" + Environment.NewLine);
                                }
                                //email file moved
                                using (CodeEmail clsEmail = new CodeEmail())
                                {
                                    clsEmail.EmailTo = "wthomas@zaxbys.com|speevy@zaxbys.com";
                                    clsEmail.FyleFrom = fyle;
                                    clsEmail.FyleTo = fyleToo;
                                    clsEmail.FileExists = bMoved;
                                    clsEmail.SendEmail();
                                }
                                //delete from fyle
                                if (!bMoved)
                                {
                                    File.Delete(fyle);
                                    File.Delete(fyle + "_SEQ");
                                }
                            }
                            catch (Exception exZax)
                            {
                                File.AppendAllText(logFyle, $@"Error: {exZax.Message}" + Environment.NewLine);
                            }
                        }
                        if (Path.GetFileName(fyle.ToUpper()).StartsWith(zflFilePrefix))
                        {
                            try
                            {
                                fyleToo = $@"{zflToDir}\{Path.GetFileName(fyle)}";
                                if (File.Exists(fyleToo))
                                {
                                    bMoved = true;
                                    File.AppendAllText(logFyle, $@"{zflFilePrefix} File NOT copied from {fyle} to {fyleToo}, moved previously." + Environment.NewLine);
                                }
                                else
                                {
                                    bMoved = false;
                                    File.Copy(fyle, fyleToo);
                                    File.AppendAllText(logFyle, $@"{zflFilePrefix} File copied from {fyle} to {fyleToo}" + Environment.NewLine);
                                }
                                //email file moved
                                using (CodeEmail clsEmail=new CodeEmail())
                                {
                                    clsEmail.EmailTo = "cdelay@zaxbys.com|jcrook@zaxbys.com";
                                    clsEmail.FyleFrom = fyle;
                                    clsEmail.FyleTo = fyleToo;
                                    clsEmail.FileExists = bMoved;
                                    clsEmail.SendEmail();
                                }
                                //delete from fyle
                                if (!bMoved)
                                {
                                    File.Delete(fyle);
                                    File.Delete(fyle + "_SEQ");
                                }
                            }
                            catch (Exception exZfl)
                            {
                                File.AppendAllText(logFyle, $@"Error: {exZfl.Message}" + Environment.NewLine);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(logFyle, $@"Error: {ex.Message}" + Environment.NewLine);
            }
            File.AppendAllText(logFyle, $@"End: {DateTime.Now}" + Environment.NewLine + Environment.NewLine);


        }
    }
}
